
public interface Einheiten {
    int bildschirmBreite = 800;
    int bildschirmHöhe = 800;
    int gegnerRand = 40;
    int gegnerReihe = 4;
    int gegnerZeile = 7;
    int geschwindigkeit = 3;
}