import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

public class Munition {

    private boolean munitionLebt = true;
    private Rectangle schuss = null;
    private Pane p;
    private int erhöhe;
    private static Rectangle rechteck = Main.pointer;

    public Munition(double x, double y, double pos, ImageView v[], Pane p) {
        if (schuss == null) {
            this.p = p;
            Timeline zeit;
            //Geschoss
            schuss = new Rectangle(x, y);
            p.getChildren().add(schuss);
            schuss.setX(pos + 35);
            schuss.setY(640);
            schuss.setFill(Color.WHITE);

            //Animation
             Duration dauer = new Duration(5);
            KeyFrame keyFrame = new KeyFrame(dauer, e -> {
                if (schuss != null) {
                    schuss.setY(schuss.getY() - 5);
                    collisionÜberprüfen(v);
                }
            });
            zeit = new Timeline(keyFrame);
            zeit.setCycleCount(Animation.INDEFINITE);
            zeit.play();
        }
    }
        //Getroffen?
    private boolean collisionÜberprüfen(ImageView gegner[]) {
        for (int i = 0; i < gegner.length; i++) {
            if (schuss != null && gegner[i] != null) {
                if ((schuss != null && schuss.getX() < gegner[i].getX() + gegner[i].getFitWidth()
                        && schuss.getX() + schuss.getWidth() > gegner[i].getX()
                        && schuss.getY() < gegner[i].getY() + gegner[i].getFitHeight()
                        && schuss.getHeight() + schuss.getY() > gegner[i].getY())) {
                    System.out.println("DENIED");
                    gegner[i].setVisible(false);
                    gegner[i] = null;
                    entferneGeschoss();
                    munitionLebt = false;
                    erhöhe += 50;
                }
            }
        }
        //Geschosse entfernen
        if (schuss != null) {
            if (schuss.getY() < 0 - schuss.getHeight() - 1) {
                entferneGeschoss();
            }
        }
        return munitionLebt;
    }
        //Punkte
    public int getScore() {
        int x = erhöhe;
        erhöhe = 0;
        return x;
    }

    public void getRectangle(Rectangle rect) {
        this.rechteck = rect;
    }

    public void entferneGeschoss() {
        p.getChildren().remove(schuss);
        schuss = null;
    }
}
