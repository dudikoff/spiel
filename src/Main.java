import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Main extends Application implements Einheiten {

    private Pane pane = new Pane();
    private Munition geschoss;
    private Image hintergrund = new Image("Hintergrund.jpg");
    private ImageView hinter = new ImageView(hintergrund);

    private Image spieler = new Image("Spieler.png");
    private ImageView schiff = new ImageView(spieler);

    private Image gegnerFrame1 = new Image("Angreifer1.jpg");
    private Image gegnerFrame2 = new Image("Angreifer2.jpg");

    static Rectangle pointer = new Rectangle();
    private ImageView[] gegner = new ImageView[gegnerZeile * gegnerReihe];

    private int bewege = 0;

    private boolean gegnerNachRechts = true;
    boolean munitionLebt = false;
    boolean newLevel = true;

    private int erzieltePunkte = 0;
    private int updateZeit = 28;
    private Text punkte = new Text("Score:" + erzieltePunkte);

    String n;

    private void nochmal() {
        for (int j = 0; j < gegnerReihe; j++) {
            for (int i = 0; i < gegnerZeile; i++) {
                gegner[j * gegnerZeile + i] = new ImageView(gegnerFrame1);
                gegner[j * gegnerZeile + i].setPreserveRatio(true);
                gegner[j * gegnerZeile + i].setX(i * 50);
                gegner[j * gegnerZeile + i].setY(j * 50);
                gegner[j * gegnerZeile + i].setFitWidth(gegnerRand);
                pane.getChildren().add(gegner[j * gegnerZeile + i]);
                if (i == gegnerZeile - 1 && j == 0) {
                    pointer.setWidth(gegnerRand);
                    pointer.setHeight(gegnerRand);
                    pointer.setFill(Color.TRANSPARENT);
                    pointer.setX(gegner[i].getX() + gegnerRand);
                }
            }
        }
        updateZeit -= 3;
    }

    @Override
    public void start(Stage primaryStage) {

        //Level erstellen
        schiff.setPreserveRatio(true);
        schiff.setFitWidth(80);
        schiff.setX(100);
        schiff.setY(680);

        pane.getChildren().add(hinter);
        pane.getChildren().add(schiff);
        pane.getChildren().add(punkte);

        //Punkte anzeigen
        punkte.setFont(Font.font("Courier New", 18));
        punkte.setFill(Color.TOMATO);

        nochmal();

        hinter.setX(0);
        hinter.setY(0);

        punkte.setX(10);
        punkte.setY(20);

        Duration dI = new Duration(updateZeit);
        KeyFrame f = new KeyFrame(dI, e -> gegnerBewegung());
        Timeline tl = new Timeline(f);
        tl.setCycleCount(Animation.INDEFINITE);
        tl.play();
        Scene scene = new Scene(pane, bildschirmBreite, bildschirmHöhe);
        scene.setOnKeyPressed(e -> tastatur(e));


        primaryStage.setScene(scene);
        primaryStage.show();


    }

    //Steuerung
    private void tastatur(KeyEvent eingabeTastatur) {

        //rechts
        if (eingabeTastatur.getCode() == KeyCode.D) {
            double x = schiff.getX();
            x += 10;
            schiff.setX(x);

            //links
        } else if (eingabeTastatur.getCode() == KeyCode.A) {
            double x = schiff.getX();
            x -= 10;
            schiff.setX(x);
        } else if (eingabeTastatur.getCode() == KeyCode.SPACE) { // Schuss
            geschoss = new Munition(10, 50, schiff.getX(), gegner, pane);
        }
    }

    //Bewegung Gegner
    private void gegnerBewegung() {
        if (gegnerNachRechts) { //geht der Gegner rechts?
            if (pointer.getX() + gegnerRand >= bildschirmBreite) { //Kollision rechts
                gegnerNachRechts = false;
                for (int i = 0; i < gegner.length; i++) {
                    if (gegner[i] != null) {
                        gegner[i].setY(gegner[i].getY() + 50);
                    }
                }
            }
            for (int i = 0; i < gegner.length; i++) {
                if (gegner[i] != null) {
                    gegner[i].setX(gegner[i].getX() + geschwindigkeit); //bewege den Gegner
                }
            }
            pointer.setX(pointer.getX() + geschwindigkeit);
        } else {
            if (pointer.getX() - ((gegnerRand * (gegnerZeile + 2))) <= 0) {
                gegnerNachRechts = true;
                for (int i = 0; i < gegner.length; i++) {
                    if (gegner[i] != null) {
                        gegner[i].setY(gegner[i].getY() + 50);
                    }
                }
            }   //Geschwindigkeit
            for (int i = 0; i < gegner.length; i++) {
                if (gegner[i] != null) {
                    gegner[i].setX(gegner[i].getX() - geschwindigkeit);
                }
            }
            pointer.setX(pointer.getX() - geschwindigkeit);
        }
        bewege++; //Animation Spielerfigut
        if (bewege == 20) { //erster Frame
            for (int j = 0; j < gegner.length; j++) {
                if (gegner[j] != null) {
                    gegner[j].setImage(gegnerFrame2);
                }
            }
        } else if (bewege == 40) { //zweiter Frame
            for (int j = 0; j < gegner.length; j++) {
                if (gegner[j] != null) {
                    gegner[j].setImage(gegnerFrame1);
                }
            }
            bewege = 0;
        }
        if (geschoss != null) {
            erzieltePunkte += geschoss.getScore();
            punkte.setText("Score: " + erzieltePunkte);
        }

    }

    public static void main(String[] args) {
        launch(args);


    }
}

